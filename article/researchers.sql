-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 03, 2018 at 08:54 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `researchers`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(100) NOT NULL,
  `a_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci NOT NULL,
  `a_thread` int(100) NOT NULL,
  `a_author` int(100) NOT NULL,
  `a_publisher` int(100) NOT NULL,
  `a_abs` text CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci NOT NULL,
  `a_key` int(100) NOT NULL,
  `a_file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci NOT NULL,
  `s_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `a_name`, `a_thread`, `a_author`, `a_publisher`, `a_abs`, `a_key`, `a_file`, `s_id`) VALUES
(1, 'سجاد سراج‌زاده', 2, 21, 212, '21212', 212122, 'iranopenuav2017_indoor_rules-v1-2.pdf', 0),
(2, 'سجاد سراج‌زاده', 2, 21, 212, '21212', 212122, 'iranopenuav2017_indoor_rules-v1-2.pdf', 0);

-- --------------------------------------------------------

--
-- Table structure for table `educate`
--

CREATE TABLE `educate` (
  `id` int(100) NOT NULL,
  `s_id` int(100) NOT NULL,
  `e_level1` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_location1` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_start1` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_end1` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_reshte1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci NOT NULL,
  `e_level2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci NOT NULL,
  `e_location2` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_start2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci NOT NULL,
  `e_end2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci NOT NULL,
  `e_reshte2` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_level3` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_location3` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_start3` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_end3` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_reshte3` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_level4` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_location4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci NOT NULL,
  `e_start4` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_end4` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `e_reshte4` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `educate`
--

INSERT INTO `educate` (`id`, `s_id`, `e_level1`, `e_location1`, `e_start1`, `e_end1`, `e_reshte1`, `e_level2`, `e_location2`, `e_start2`, `e_end2`, `e_reshte2`, `e_level3`, `e_location3`, `e_start3`, `e_end3`, `e_reshte3`, `e_level4`, `e_location4`, `e_start4`, `e_end4`, `e_reshte4`, `time`) VALUES
(1, 0, 'کاردانی', '1', '1', '11', '1', 'کارشناسی', '1', '1', '1', '1', 'کارشناسی ارشد', '1', '1', '1', '1', 'دکتری', '1', '1', '1', '1', '2018-02-03 19:22:54');

-- --------------------------------------------------------

--
-- Table structure for table `invention`
--

CREATE TABLE `invention` (
  `id` int(100) NOT NULL,
  `s_id` int(100) NOT NULL,
  `i_name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `i_date` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `i_text` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `invention`
--

INSERT INTO `invention` (`id`, `s_id`, `i_name`, `i_date`, `i_text`, `time`) VALUES
(1, 0, '1', '1', '1', '2018-02-03 19:32:32');

-- --------------------------------------------------------

--
-- Table structure for table `lecture`
--

CREATE TABLE `lecture` (
  `id` int(100) NOT NULL,
  `s_id` int(100) NOT NULL,
  `l_type1` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `l_mcon` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `l_conf` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `l_type2` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `l_mjor` varchar(255) CHARACTER SET utf32 COLLATE utf32_persian_ci NOT NULL,
  `l_jor` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `lecture`
--

INSERT INTO `lecture` (`id`, `s_id`, `l_type1`, `l_mcon`, `l_conf`, `l_type2`, `l_mjor`, `l_jor`, `time`) VALUES
(1, 0, 'کنفرانس', '', '', 'ژورنال', '', '', '2018-02-03 19:28:24'),
(2, 0, 'کنفرانس', '1', '1', 'ژورنال', '11', '1', '2018-02-03 19:28:36'),
(3, 0, 'کنفرانس', '1', '1', 'ژورنال', '11', '1', '2018-02-03 19:29:12'),
(4, 0, 'کنفرانس', '1', '11', 'ژورنال', '11', '1', '2018-02-03 19:29:20');

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE `manager` (
  `id` int(11) NOT NULL,
  `user` varchar(250) COLLATE utf8_persian_ci NOT NULL,
  `pass` varchar(250) COLLATE utf8_persian_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `manager`
--

INSERT INTO `manager` (`id`, `user`, `pass`, `time`) VALUES
(1, 'admin', 'admin', '2018-02-03 19:39:41');

-- --------------------------------------------------------

--
-- Table structure for table `prize`
--

CREATE TABLE `prize` (
  `id` int(100) NOT NULL,
  `s_id` int(100) NOT NULL,
  `pr_name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `pr_date` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `pr_text` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `prize`
--

INSERT INTO `prize` (`id`, `s_id`, `pr_name`, `pr_date`, `pr_text`, `time`) VALUES
(1, 0, '11', '11', '11', '2018-02-03 19:34:30');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(100) NOT NULL,
  `s_id` int(100) NOT NULL,
  `p_tarh` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `p_pro` varchar(255) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `s_id`, `p_tarh`, `p_pro`) VALUES
(1, 0, '۱', '۱'),
(2, 0, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `scholar`
--

CREATE TABLE `scholar` (
  `id` int(11) NOT NULL,
  `s_name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `s_family` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `s_code` int(11) NOT NULL,
  `s_mob` int(11) NOT NULL,
  `s_user` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `s_pass` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `scholar`
--

INSERT INTO `scholar` (`id`, `s_name`, `s_family`, `s_code`, `s_mob`, `s_user`, `s_pass`, `time`) VALUES
(1, '2', '1', 2, 2, '2', '2', '2018-02-03 15:52:43'),
(2, '۱', '۱', 0, 0, '۱', '۱', '2018-02-03 15:54:01');

-- --------------------------------------------------------

--
-- Table structure for table `trans`
--

CREATE TABLE `trans` (
  `id` int(11) NOT NULL,
  `s_id` int(100) NOT NULL,
  `t_name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `t_date` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `t_publisher` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `t_author` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `t_year` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `educate`
--
ALTER TABLE `educate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invention`
--
ALTER TABLE `invention`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecture`
--
ALTER TABLE `lecture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manager`
--
ALTER TABLE `manager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prize`
--
ALTER TABLE `prize`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar`
--
ALTER TABLE `scholar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `s_code` (`s_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `educate`
--
ALTER TABLE `educate`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `invention`
--
ALTER TABLE `invention`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lecture`
--
ALTER TABLE `lecture`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `manager`
--
ALTER TABLE `manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `prize`
--
ALTER TABLE `prize`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `scholar`
--
ALTER TABLE `scholar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
